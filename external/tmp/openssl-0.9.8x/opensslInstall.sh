#!/bin/bash 

export TOOLCHAIN=/opt/freescale/usr/local/gcc-4.4.4-glibc-2.11.1-multilib-1.0/arm-fsl-linux-gnueabi
export CC=$TOOLCHAIN/bin/arm-none-linux-gnueabi-gcc
export RANLIB=$TOOLCHAIN/bin/arm-none-linux-gnueabi-ranlib
./Configure --prefix=/ linux-generic32 shared
make -j4
sudo make INSTALL_PREFIX=$TOOLCHAIN install_sw
